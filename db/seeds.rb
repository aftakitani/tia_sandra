# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Recipe.create(name: 'Bolo de chocolate',
              directions: 'Junte tudo',
              ingredients: 'Farinha, chocolate e manteiga',
              recipe_type: 'doce')


Recipe.create(name: 'Pão de queijo',
              directions: 'Misture o queijo, e o pão',
              ingredients: 'Pão, queijo e bacon',
              recipe_type: 'salgado')