class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :name
      t.text :directions
      t.string :ingredients
      t.string :recipe_type

      t.timestamps
    end
  end
end
