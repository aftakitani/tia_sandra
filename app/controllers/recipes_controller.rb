class RecipesController < ApplicationController
  def new
  end

  def create
    @recipe = Recipe.create(name: params["name"], directions: params[:directions])
    redirect_to :home_index
  end

  def show
    @recipe = Recipe.find(params[:id])
  end

end
