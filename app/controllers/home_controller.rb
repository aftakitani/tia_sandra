class HomeController < ApplicationController
  def index
    @receitas = Recipe.order(created_at: :desc).limit(10)
  end
end
